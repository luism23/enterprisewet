import Link from "next/link"


const LinkInput = ({ link = "", text = "", color = "" }) => {
    return (
        <>
            <div className="flex ">
                <Link href={link}>
                    <a className={`m-r-15 width-230 flex flex-justify-center font-poppins font-14 font-w-700  border-radius-50 flex   p-v-17 p-h-56 ${color}`}>
                        {text}
                    </a>

                </Link>
            </div>
        </>
    )
}


const Index = ({ img = "", title = "", text = "", linkInput = [] }) => {
    return (
        <>
            <div className="flex bg-white-1 m-t-100">
                <div className="flex container p-h-15 flex-align-center m-t-200 m-md-t-128 m-b-128">
                    <div className="flex-12 flex-md-6 flex">
                        <h1 className="flex font-w-700 font-poppins font-54 color-gray-1 m-b-24 ">
                            {title}
                        </h1>
                        <p className="font-28 font-poppins color-gray-2 m-b-48">
                            {text}
                        </p>
                        <div className="flex">
                            {
                                linkInput.map((e, i) => {
                                    return (
                                        <LinkInput
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>
                    </div>
                    <div className="flex-12 flex-md-6 flex flex-justify-center">
                        <img className="flex" src={`/image/${img}`} alt="" />
                    </div>
                </div>
            </div>

        </>
    );
};
export default Index;
