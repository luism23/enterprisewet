
const ImgContent = ({ img = "" }) => {
    return (
        <>
            <div className="">
                <img className="flex" src={`/image/${img}`} alt="" />
            </div>
        </>
    )
}


const Index = ({ imgContent = [] }) => {
    return (
        <>
            <div className="bg-white-1 m-b-45">
                <div className="container p-h-15 flex flex-justify-center ">
                    {
                        imgContent.map((e, i) => {
                            return (
                                <ImgContent
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </div>

        </>
    )
}
export default Index