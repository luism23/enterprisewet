
const AddText = ({ title = "", text = "" }) => {
    return (
        <>
            <div className="m-t-64  flex-12 flex-md-6  border-radius-20 border-0 bg-white-2 p-v-35 p-h-20">
                <h3 className="color-black-1 font-24 font-poppins font-w-600 m-b-20">
                    {title}
                </h3>
                <p className="color-black-1 font-16 font-poppins">
                    {text}
                </p>
            </div>
        </>
    )
}

const Index = ({ title = "", addText = [] }) => {
    return (
        <>
            <div className="bg-white-1 ">
                <div className="container p-h-15  p-v-58 ">
                    <div className="text-center">
                        <h1 className="color-blue-1 font-32 font-poppins font-w-600">
                            {title}
                        </h1>

                    </div>
                    <div className=" ">
                        <div className="flex ">
                            {
                                addText.map((e, i) => {
                                    return (
                                        <AddText
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}
export default Index