import Link from "next/link"

import Data from "@/data/components/Header"
import { useEffect, useRef, useState } from "react"
import Menu from "@/svg/menu"

const LinkInput = ({ link = "", text = "", className = "" }) => {
    return (
        <>
            <div className="flex m-auto">
                <Link href={link}>
                    <a className={`inputHeader max-width-p-100 font-14 font-poppins font-w-700 width-110 m-l-10 p-h-22 text-center p-v-18 m-r-20 bg-yellow-1 color-white border-radius-50 border-style-solid border-1 width-185 ${className}`}>
                        {text}
                    </a>
                </Link>
            </div>
        </>
    )
}


const LinkTitle = ({ title = "", link = "" }) => {
    return (
        <>
            <div className="m-auto">
                <Link href={link}>
                    <a className="font-14 font-poppins font-w-700  color-black ">
                        {title}
                    </a>
                </Link>
            </div>
        </>
    )
}



const Index = () => {
    const { linkTitle, linkInput, img } = Data
    const header = useRef(null)
    const [heightMenu, setHeightMenu] = useState(0)
    useEffect(() => {
        const height = header.current.offsetHeight
        setHeightMenu(height)
    }, [])
    return (
        <>
            <header ref={header} className="header bg-white-1  pos-f width-p-100 top-0 left-0 z-index-9 flex  p-h-15 p-v-32 ">
                <div className="container flex">
                    <div className="flex-2 m-auto flex">
                        <img className="flex" src={`/image/${img}`} alt="" />
                    </div>
                    <div className="border-radius-50 flex-10 d-lg-none flex flex-justify-right">
                        <button
                            className="color-white bg-transparent border-0"
                            onClick={() => {
                                document.body.classList.toggle("activeMenu")
                            }}>
                            <Menu size={34} className="border-radius-50" />
                        </button>
                    </div>
                    <div
                        className="menuMovil flex-justify-center bg-white-1 flex-12 flex-lg-10 flex"
                        style={{ "--heightMenu": heightMenu + "px" }}
                        onClick={(e) => {
                            if (e.target.tagName == "A") {
                                document.body.classList.toggle("activeMenu")
                            }
                        }}
                    >
                        <div className="flex-12 col-1 flex-justify-right flex-lg-6 flex ">
                            {
                                linkTitle.map((e, i) => {
                                    return (
                                        <LinkTitle
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>
                        <div className="flex-12  flex-lg-3 flex ">
                            {
                                linkInput.map((e, i) => {
                                    return (
                                        <LinkInput
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>
                    </div>


                </div>
            </header>
        </>
    )
}
export default Index