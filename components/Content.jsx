import Header from "@/components/Header";
import Footer from "@/components/Footer";

const Index = ({ children, header = true, footer = true }) => {
    return (
        <>
            {header && <Header />}
            <div className="content">{children}</div>
            {footer && <Footer />}
        </>
    );
};
export default Index;
