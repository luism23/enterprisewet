import Img from '@/components/Img';

const Index = ({ src = '', name = '', className = '', classNameImg = '', capas = []}) => {
  return (
    <>
      <Img
        src={src}
        name={name}
        className={`bg ${className}`}
        classNameImg={`bg-img ${classNameImg}`}
        capas={capas}
      />
    </>
  );
};
export default Index;
