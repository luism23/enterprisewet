import Content from "@/components/Content"
import Banner from "@/components/banner"
import ImgNot from "@/components/imgNot"
import ContentInfo from "@/components/contentInfo"

import Info from "@/data/pages/index"

const Index = () => {
    return (
        <>
            <Content>
                <Banner {...Info.banner}/>
                <ImgNot {...Info.imgNot}/>
                <ContentInfo {...Info.contentInfo}/>
            </Content>
        </>
    )
}
export default Index