export default {
    banner: {
        title: "No Code No Compromises",
        text: "EnterpriseWeb is the first industrial-grade no-code platform specifically designed for software engineers. ",
        linkInput: [
            {
                link: "/",
                text: "Register Now",
                color: "bg-yellow-1  color-white ",
            },
            {
                link: "/pricing",
                text:"Get A Quote",
                color: "bg-white-2 color-black ",

            }
        ],
        img: "banner.png",
    },
    imgNot:{
        imgContent:[
            {
                img:"img1.png"
            },
            {
                img:"img2.png"
            },
            {
                img:"img3.png"
            },
            {
                img:"img4.png"
            },
        ]
    },
    contentInfo:{
        title:"Enabling the real-time Digital Business",
        addText:[
            {
                title:"Application Modernization:",
                text:"Simplify the design, deployment and management of stateful Cloud-native applications to ease & accelerate service delivery ",
            },
            {
                title:"Business Transformation: ",
                text:"Rapidly model end-to-end, event-driven processes across business silos, cloud services and eco-system partners for highly-automated & agile operations",
            },
        ]
    }
}