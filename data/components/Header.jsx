export default {
    img: "logo.png",
    linkTitle: [
        {
            link: "/",
            title: "Home",
        },
        {
            link: "/",
            title: "Platform",
        },
        {
            link: "/",
            title: "Solutions",
        },
        {
            link: "/",
            title: "Partners",
        },
        {
            link: "/",
            title: "News & Events",
        },
        {
            link: "//",
            title: "Company",
        },
        {
            link: "//",
            title: "Contact Us",
        },
    ],
    linkInput: [
        {
            link: "/",
            text: "Register Now",
        },

    ],
}